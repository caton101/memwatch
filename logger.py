# BSD 3-Clause License
#
# Copyright (c) 2022, Cameron Himes
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import subprocess
from time import sleep
import sqlite3
from hashlib import sha256

TIMEOUT = 1
DATABASE_FILE = "ps.db"
MASTER_TABLE_NAME = "master"
PS_COMMAND = ["ps", "aux"]

def getProcessList():
    process_list = subprocess.run(PS_COMMAND, stdout=subprocess.PIPE)
    cleaned_list = []
    for i in process_list.stdout.decode().split("\n"):
        chopped = i.split()
        if len(chopped) == 0:
            continue
        cleaned_list.append({
            "user": chopped[0],
            "pid": chopped[1],
            "cpu": chopped[2],
            "mem": chopped[3],
            "vsz": chopped[4],
            "rss": chopped[5],
            "tty": chopped[6],
            "stat": chopped[7],
            "start": chopped[8],
            "time": chopped[9],
            "command": chopped[10]
        })
    return cleaned_list


def writeTable(data, database):
    # open connection to database
    cursor = sqlite3.connect(database=database)
    # make and store tables for each process
    cursor.execute(
        f'CREATE TABLE IF NOT EXISTS {MASTER_TABLE_NAME} (ID TEXT NOT NULL)')
    for data_row in data:
        data_string = f'{data_row["pid"]} {data_row["command"]}'
        data_hash = "SHA256_" + sha256(data_string.encode()).digest().hex()
        cursor.execute(
            f'INSERT INTO {MASTER_TABLE_NAME} VALUES (\"{data_hash}\")')
        cursor.execute(
            f'CREATE TABLE IF NOT EXISTS {data_hash} (USER TEXT NOT NULL, PID TEXT NOT NULL, CPU TEXT NOT NULL, MEM TEXT NOT NULL, VSZ TEXT NOT NULL, RSS TEXT NOT NULL, TTY TEXT NOT NULL, STAT TEXT NOT NULL, START TEXT NOT NULL, TIME TEXT NOT NULL, COMMAND TEXT NOT NULL)')
        cursor.execute(
            f'INSERT INTO \"{data_hash}\" VALUES (\"{data_row["user"]}\", \"{data_row["pid"]}\", \"{data_row["cpu"]}\", \"{data_row["mem"]}\", \"{data_row["vsz"]}\", \"{data_row["rss"]}\", \"{data_row["tty"]}\", \"{data_row["stat"]}\", \"{data_row["start"]}\", \"{data_row["time"]}\", \"{data_row["command"]}\")')
    cursor.commit()
    cursor.close()


def main():
    while True:
        processes = getProcessList()
        print(processes)
        writeTable(processes, DATABASE_FILE)
        sleep(TIMEOUT)


if __name__ == "__main__":
    main()
