# BSD 3-Clause License
#
# Copyright (c) 2022, Cameron Himes
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sqlite3
import matplotlib.pyplot as plt

DATABASE_FILE = "ps.db"
MASTER_TABLE_NAME = "master"
MEMORY_MIN_LOG = 0.1
MAX_FLAGGED = 10
FLAGGED = []  # this is set at runtime


def plotProcess(pid, cpu, mem, cmd):
    print(cpu, mem)
    plt.cla()
    plt.clf()
    plt.plot(cpu, label="CPU")
    plt.plot(mem, label="MEM")
    plt.title(f'{pid}: {cmd}')
    plt.legend()
    plt.savefig(f'./({pid}) {cmd.replace("/", "+")}.jpg')


def processCommand(response):
    global FLAGGED
    cpu_history = []
    mem_history = []
    skip = True
    process_id = response[0][0]
    command_str = response[0][3]
    if command_str == "COMMAND":
        return
    print("Processing", command_str)
    for _, cpu, mem, _ in response:
        cpu_history.append(float(cpu))
        mem_history.append(float(mem))
        if float(mem) > MEMORY_MIN_LOG:
            skip = False
    if skip:
        print("Skipping", command_str)
        return
    max_mem = max(mem_history)
    flag_str = f'({process_id}) {command_str}'
    process_tuple = (max_mem, flag_str)
    if len(FLAGGED) < MAX_FLAGGED:
        if process_tuple not in FLAGGED:
            FLAGGED.append(process_tuple)
            FLAGGED.sort()
    elif len(FLAGGED) > 0 and max_mem > FLAGGED[0][0]:
        if process_tuple not in FLAGGED:
            FLAGGED.append(process_tuple)
            FLAGGED.sort()
            FLAGGED.pop(0)
    while len(FLAGGED) > MAX_FLAGGED:
        FLAGGED.pop(0)
    plotProcess(process_id, cpu_history, mem_history, command_str)


def parseTable(database):
    # open connection to database
    connection = sqlite3.connect(database=database)
    # get a list of table IDs
    cursor = connection.execute(f'SELECT * FROM {MASTER_TABLE_NAME}')
    response = cursor.fetchall()
    # calculate CPU and MEMORY usage for each process
    for table_hash in response:
        cursor = connection.execute(
            f'SELECT PID, CPU, MEM, COMMAND FROM {table_hash[0]}')
        response = cursor.fetchall()
        processCommand(response)
    connection.close()


def main():
    parseTable(DATABASE_FILE)
    print(FLAGGED)
    print(f'The top {MAX_FLAGGED} processes:')
    for mem, cmd in FLAGGED:
        print(f'\t{mem}%: {cmd}')


if __name__ == "__main__":
    main()
