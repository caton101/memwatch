# MemWatch

A set of programs for monitoring processes over time and generating charts.
This is useful for finding memory leaks.

## Disclaimer

This program is not meant for mass production. This is a crude implementation
of a tool I needed for a one-time event. I have no further plans to add more
features or publish this program.

## Dependencies

* Python 3
* Matplotlib
* SQLite3
* A UNIX-Like operating system

## How to use

1. Run `logger.py` until you are done collecting information. (NOTE: You can
change the database file and the command that runs at the top of the file.)
2. Run `export.py` to generate charts.
3. (optional) Check the top 10 programs mentioned by `export.py` to see if
there was a memory leak.
